import { defineStore } from 'pinia';

export const useCountryStore = defineStore('countryStore', ({
    state: () => ({
        countries: [
            {"name":"Kyrgyzstan","topLevelDomain":[".kg"],"alpha2Code":"KG","alpha3Code":"KGZ","callingCodes":["996"],"capital":"Bishkek","altSpellings":["KG","Киргизия","Kyrgyz Republic","Кыргыз Республикасы","Kyrgyz Respublikasy"],"subregion":"Central Asia","region":"Asia","population":6591600,"latlng":[41.0,75.0],"demonym":"Kirghiz","area":199951.0,"gini":29.7,"timezones":["UTC+06:00"],"borders":["CHN","KAZ","TJK","UZB"],"nativeName":"Кыргызстан","numericCode":"417","flags":{"svg":"https://flagcdn.com/kg.svg","png":"https://flagcdn.com/w320/kg.png"},"currencies":[{"code":"KGS","name":"Kyrgyzstani som","symbol":"с"}],"languages":[{"iso639_1":"ky","iso639_2":"kir","name":"Kyrgyz","nativeName":"Кыргызча"},{"iso639_1":"ru","iso639_2":"rus","name":"Russian","nativeName":"Русский"}],"translations":{"br":"Kirgizstan","pt":"Quirguizistão","nl":"Kirgizië","hr":"Kirgistan","fa":"قرقیزستان","de":"Kirgisistan","es":"Kirguizistán","fr":"Kirghizistan","ja":"キルギス","it":"Kirghizistan","hu":"Kirgizisztán"},"flag":"https://flagcdn.com/kg.svg","regionalBlocs":[{"acronym":"EEU","name":"Eurasian Economic Union","otherAcronyms":["EAEU"]}],"cioc":"KGZ","independent":true,"isVisited":true},
            {"name":"Israel","topLevelDomain":[".il"],"alpha2Code":"IL","alpha3Code":"ISR","callingCodes":["972"],"capital":"Jerusalem","altSpellings":["IL","State of Israel","Medīnat Yisrā'el"],"subregion":"Western Asia","region":"Asia","population":9216900,"latlng":[31.5,34.75],"demonym":"Israeli","area":20770.0,"gini":39.0,"timezones":["UTC+02:00"],"borders":["EGY","JOR","LBN","SYR"],"nativeName":"יִשְׂרָאֵל","numericCode":"376","flags":{"svg":"https://flagcdn.com/il.svg","png":"https://flagcdn.com/w320/il.png"},"currencies":[{"code":"ILS","name":"Israeli new shekel","symbol":"₪"}],"languages":[{"iso639_1":"he","iso639_2":"heb","name":"Hebrew (modern)","nativeName":"עברית"},{"iso639_1":"ar","iso639_2":"ara","name":"Arabic","nativeName":"العربية"}],"translations":{"br":"Israel","pt":"Israel","nl":"Israël","hr":"Izrael","fa":"اسرائیل","de":"Israel","es":"Israel","fr":"Israël","ja":"イスラエル","it":"Israele","hu":"Izrael"},"flag":"https://flagcdn.com/il.svg","cioc":"ISR","independent":true,"isVisited":false},
        ],
        isSearch: false
    }),
    getters: {
        visitedCountries() {
            return this.countries.filter(country => country.isVisited)
        }
    },
    actions: {
        changeIsSearch(bool) {
            this.isSearch = bool
        },
        toggleVisited(alpha3Code) {
            console.log(alpha3Code);
            const idx = this.countries.findIndex(el => el.alpha3Code == alpha3Code);
            this.countries[idx].isVisited = !this.countries[idx].isVisited;
        },
        deleteCountr(id) {
            const idx = this.countries.findIndex(el => el.id == id);
            this.countries.splice(idx, 1);
        }
    }
}))
