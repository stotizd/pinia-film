import { defineStore } from "pinia";
const url = "https://restcountries.com/v2/capital/";
export const useSearchStore = defineStore('searchStore', ({
    state: () => ({
        countries: null
    }),
    actions: {
        async getCountries(capital) {
            const res = await fetch(`${url}${capital}`);
            const data = await res.json();
            this.countries = data
        }
    }
}));